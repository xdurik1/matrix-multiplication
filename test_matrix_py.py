import matrix as m
import pytest

@pytest.fixture(scope='module')
def setup():
    setup = m.Matrix(2,3)
    setup.values.append([1,2])
    setup.values.append([5,3])
    setup.values.append([6,7])
    return setup

@pytest.fixture(scope='module')
def setup1():
    setup1 = m.Matrix(1,2)
    setup1.values.append([5])
    setup1.values.append([1])
    return setup1


def test_get_width(setup):
    assert setup.get_width() == 2
    assert type(setup.get_width()) == int

def test_get_heigt(setup):
    assert setup.get_height() == 3
    assert type(setup.get_height()) == int

def test_multi(setup,setup1):
    result = [[7],[28],[37]]
    assert setup.multi(setup1) == result

def test_get_value(setup):
    assert setup.get_value(1,1) == 3

def test_set_value(setup):
    assert setup.values[1][1] != 100000000
    setup.set_value(1, 1, 100)
    assert setup.values[1][1] == 100
