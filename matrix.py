import  sys

class Matrix:
    def __init__(self,width=None, height=None):
        try:
            if not width:
                self.width = int(input('width: '))
            else:
                self.width = width
            if not height:
                self.height = int(input('height: '))
            else:
                self.height = height
            self.values = []
        except Exception as error:
            print('Caught this error: ' + repr(error))
            sys.exit()

    #adding values of matrix
    def add_values(self):
        for x in range(self.height):
            try:
                inp = [int(x) for x in input().split()]
                if len(inp)> self.width:
                    self.values.append(inp[:self.width])
                    print("To much values was entered, only the necessary values ​​are stored")

                elif len(inp)< self.width:
                    while len(inp)<  self.width:
                        inp.append(0)
                    print("Missing values ​​were replaced by number 0")
                    self.values.append(inp[:self.width])
                else:
                    self.values.append(inp[:self.width])

            except Exception as error:
                print('Caught this error: ' + repr(error))
                sys.exit()

    def get_height(self):
        return int(self.height)

    def get_width(self):
        return int(self.width)

    def set_value(self, a, b, c):
        self.values[a][b] = c

    def get_value(self, a, b):
        return int(self.values[a][b])

    # Calculation of the result
    def multi(self,another):
        C = [[0 for x in range(another.get_width())] for y in range(self.get_height())]
        for i in range(self.get_height()):
            for j in range(another.get_width()):
                for k in range(another.get_height()):
                    C[i][j] += self.get_value(i, k) * another.get_value(k, j)
        return C
