import matrix as m

print("Matrix A")
a = m.Matrix()
print("\nMatrix B")
b = m.Matrix()

print("\nMatrix A values: ")
a.add_values()
print("\nMatrix B values: ")
b.add_values()

print("\nResult: ")

for x in a.multi(b):
    for xx in x:
        print(xx)